<?php
namespace App\Modules\Admins\Controllers\Admin;

use HZ\Illuminate\Mongez\Http\RestfulApiController;

class AdminsController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'admins',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [
                'name' => 'required|min:2',
                'email' => 'required|email',
                'password' => 'required|min:5|max:12',
                'password_conformation' => 'required|same:password',
            ],
            'store' => [],
            'update' => [],
            'patch' => [],
        ],
    ];
}
