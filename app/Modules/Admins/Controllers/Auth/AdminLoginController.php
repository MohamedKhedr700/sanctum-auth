<?php
namespace App\Modules\Admins\Controllers\Auth;

use HZ\Illuminate\Mongez\Http\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;

class AdminLoginController extends ApiController
{

    /**
     * Repository name
     *
     * @var string
     */
    public const REPOSITORY_NAME = 'admins';
    /**
     * admin login
     *
     * @param Request $request
     * @return false|string
     */
    public function login(Request $request)
    {
        if ($admin = $this->repository->getByModel('email', \request('email'))) {
            if (Hash::check(\request('password'), $admin->password)) {
                return $this->success(['accessToken' => $admin->createToken($admin->email)->plainTextToken]);
            }
        }
        return  $this->badRequest((new MessageBag())->add('email', trans('auth.failed')));
    }
}
