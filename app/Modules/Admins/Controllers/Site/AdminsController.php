<?php
namespace App\Modules\Admins\Controllers\Site;

use Illuminate\Http\Request;
use HZ\Illuminate\Mongez\Http\ApiController;

class AdminsController extends ApiController
{
    /**
     * Repository name
     * 
     * @var string
     */
    public const REPOSITORY_NAME = 'admins';

    /**
     * {@inheritDoc}
     */
    public function index(Request $request)
    {
        $options = [];

        return $this->success([
            'records' => $this->repository->listPublished($options),
        ]);
    }
    
    /**
     * {@inheritDoc}
     */
    public function show($id, Request $request)
    {
        $record = $this->repository->getPublished($id);

        if (! $record) {
            return $this->notFound('notFoundRecord');
        }

        return $this->success([
            'record' => $record,
        ]);
    }
}