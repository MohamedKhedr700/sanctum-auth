<?php

use App\Modules\Admins\Controllers\Auth\AdminLoginController;
use App\Modules\Admins\Controllers\Auth\AdminRegisterController;
use Illuminate\Support\Facades\Route;
use App\Modules\Admins\Controllers\Site\AdminsController;

/*
|--------------------------------------------------------------------------
| Admins Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your main "front office" application.
| Please note that this file is auto imported in the main routes file, so it will inherit the main "prefix"
| and "namespace", so don't edit it to add for example "api" as a prefix.
*/


Route::group([
    'prefix' => 'admins'
], function () {
    Route::post('login', [AdminLoginController::class, 'login']);
    Route::post('register', [AdminRegisterController::class, 'register']);
});

Route::group([
    'prefix' => 'admins', 'middleware' => 'auth:adminApi'
], function () {
    // Sub API routes DO NOT remove this line
    // list records
    Route::get('admins', [AdminsController::class, 'index']);
    // one record
    Route::get('admins/{id}', [AdminsController::class, 'show']);
});
