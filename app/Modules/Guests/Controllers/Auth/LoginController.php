<?php
namespace App\Modules\Guests\Controllers\Auth;

use HZ\Illuminate\Mongez\Http\ApiController;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends ApiController
{
    /**
     * Repository name
     *
     * @var string
     */
    public const REPOSITORY_NAME = 'guests';


    public function login(Request $request)
    {
        $data = $request->only(['deviceId', 'deviceType', 'ip', 'userAgent']);
        if (!($guest = $this->repository->getByModel('deviceId', $data['deviceId']))) {
            $guest = $this->repository->create($data);
        }
        return $this->success(['accessToken' => $guest->createToken($guest->deviceId, ['user:login', 'user:register'])->plainTextToken]);


    }
}
