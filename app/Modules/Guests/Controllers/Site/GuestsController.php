<?php
namespace App\Modules\Guests\Controllers\Site;

use Illuminate\Http\Request;
use HZ\Illuminate\Mongez\Http\ApiController;

class GuestsController extends ApiController
{
    /**
     * Repository name
     * 
     * @var string
     */
    public const REPOSITORY_NAME = 'guests';

    /**
     * {@inheritDoc}
     */
    public function index(Request $request)
    {
        $options = [];

        return $this->success([
            'records' => $this->repository->listPublished($options),
        ]);
    }
    
    /**
     * {@inheritDoc}
     */
    public function show($id, Request $request)
    {
        $record = $this->repository->getPublished($id);

        if (! $record) {
            return $this->notFound('notFoundRecord');
        }

        return $this->success([
            'record' => $record,
        ]);
    }
}