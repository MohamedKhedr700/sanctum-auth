<?php

namespace App\Modules\Guests\Middleware;

use Closure;
use Auth as BaseAuth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApiAuth
{
    /**
     * Application key
     *
     * @var string
     */
    protected $apiKey;

    /**
     * {@inheritDoc}
     */
    public function handle(Request $request, Closure $next)
    {
        $this->apiKey = config('app.api-key');
        if ($request->authorizationValue() !== $this->apiKey) {
            return response([
                'error' => 'Invalid API Key',
            ], Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
