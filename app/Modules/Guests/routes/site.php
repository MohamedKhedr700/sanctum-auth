<?php

use App\Modules\Guests\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Modules\Guests\Controllers\Site\GuestsController;

/*
|--------------------------------------------------------------------------
| Guests Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your main "front office" application.
| Please note that this file is auto imported in the main routes file, so it will inherit the main "prefix"
| and "namespace", so don't edit it to add for example "api" as a prefix.
*/
Route::group([
], function () {
    // Sub API routes DO NOT remove this line
    // list records
    Route::get('guests', [GuestsController::class, 'index']);
    // one record
    Route::get('guests/{id}', [GuestsController::class, 'show']);

});
//guest Login
Route::post('guests/login', [LoginController::class, 'login'])->middleware('api-auth');

