<?php
namespace App\Modules\Posts\Controllers\Admin;

use HZ\Illuminate\Mongez\Http\RestfulApiController;

class PostsController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'posts',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [
                'title' => ['string'],
                'body' => ['string'],
                'image' => ['image', 'mimes:jpeg,jpg,png', 'max:5000'],
            ],
            'store' => [
                'title' => ['required'],
                'body' => ['required'],
                'image' => ['required'],
            ],
            'update' => [],
            'patch' => [],
        ],
    ];
}
