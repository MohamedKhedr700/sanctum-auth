<?php
namespace App\Modules\Posts\Filters;

use HZ\Illuminate\Mongez\Database\Filters\MongoDBFilter;

class PostsFilter extends MongoDBFilter
{
    /**
     * List with all filter.
     *
     * filterName => functionName
     * @const array
     */
    const FILTER_MAP = [];
}
