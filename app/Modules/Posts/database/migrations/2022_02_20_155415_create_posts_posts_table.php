<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            // this is very important to create a unique index for the id
            $table->unique('id');

            $table->int('id');

            $table->increments('id');

            // columns
            $table->string('userId');        
            $table->string('title');        
            $table->string('body');        
            $table->string('image');        

            // indexes

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
