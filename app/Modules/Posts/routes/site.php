<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Posts\Controllers\Site\PostsController;

/*
|--------------------------------------------------------------------------
| Posts Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your main "front office" application.
| Please note that this file is auto imported in the main routes file, so it will inherit the main "prefix"
| and "namespace", so don't edit it to add for example "api" as a prefix.
*/

Route::group([
    'middleware' => ['auth:api,guest'],
], function () {
    // Sub API routes DO NOT remove this line
    // list records
    Route::get('posts', [PostsController::class, 'index']);
    // one record
    Route::get('posts/{id}', [PostsController::class, 'show']);
});
