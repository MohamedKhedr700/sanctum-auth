<?php

namespace App\Modules\Users\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\Users\Requests\UserLoginRequest;
use HZ\Illuminate\Mongez\Http\ApiController;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;

class LoginController extends ApiController
{
    /**
     * Repository name
     *
     * @var string
     */
    public const REPOSITORY_NAME = 'users';

    public function login(Request $request)
    {
        $data = $request->only([ 'email', 'password']);
        if ($user = $this->repository->getByModel('email', $data['email'])) {
            if (Hash::check($data['password'], $user->password)) {
                return $this->success(['accessToken' => $user->createToken($user->email)->plainTextToken]);
            }
        }
        return  $this->badRequest((new MessageBag())->add('email', trans('auth.failed')));
    }
}
