<?php

namespace App\Modules\Users\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\Users\Requests\UserLoginRequest;
use HZ\Illuminate\Mongez\Http\ApiController;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;

class LogoutController extends ApiController
{
    /**
     * Repository name
     *
     * @var string
     */
    public const REPOSITORY_NAME = 'users';

    public function logout()
    {
      return user()->currentAccessToken()->delete() ? $this->success() :  $this->badRequest((new MessageBag())->add('auth', trans('auth.logout-fail')));
    }
}
