<?php

namespace App\Modules\Users\Controllers\Auth;

use App\Http\Controllers\Controller;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends RestfulApiController
{
    protected $abilities = [
        'user:logout',
        'post:create',
        'post:update',
        'post:index',
        'post:delete',
        'post:patch',
    ];
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'users',
        'listOptions' => [
            'select' =>[],
            'paginate' => null,
        ],
        'rules' => [
            'all' => [],
            'store' => [
                'email' => ['required', 'unique:users,email', 'email', 'max:255'],
                'name' => ['required', 'string', 'max:255'],
                'password' => ['required', 'string', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[&-_.!@#$%*?+~])/'],
                'image' => ['image', 'mimes:jpeg,jpg,png', 'max:5000'],
                'gender' => ['required', 'in:male,female'],
            ],
            'update' => [],
            'patch' => [],
        ],
    ];

    public function register(Request $request)
    {
        $data = $request->only(['name', 'email', 'password', 'gender']);
        $userResponse = $this->store($request);
        switch (true) {
            case $userResponse->status() == Response::HTTP_BAD_REQUEST:
                return $userResponse;
            case $userResponse->status() == Response::HTTP_CREATED:
                $user = $this->repository->getByModel('email', $data['email']);
                return $this->successCreate(['accessToken' => $user->createToken($user->email, $this->abilities)->plainTextToken]);
        }
    }
}
