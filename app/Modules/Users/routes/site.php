<?php

use App\Modules\Users\Controllers\Auth\LoginController;
use App\Modules\Users\Controllers\Auth\LogoutController;
use App\Modules\Users\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;
use App\Modules\Users\Controllers\Site\UsersController;

/*
|--------------------------------------------------------------------------
| Users Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your main "front office" application.
| Please note that this file is auto imported in the main routes file, so it will inherit the main "prefix"
| and "namespace", so don't edit it to add for example "api" as a prefix.
*/

Route::group([
    'prefix' => 'users', 'middleware' => ['auth:guest,api', 'ability:user:login,user:register,user:logout'],
], function () {
    Route::post('register', [RegisterController::class, 'register']);
    Route::post('login', [LoginController::class, 'login']);
    Route::post('logout', [LogoutController::class, 'logout']);
});

Route::group([
    'prefix' => 'users', 'middleware' => 'auth:api'
], function () {
    // Sub API routes DO NOT remove this line
    Route::get('profile', [UsersController::class, 'authorized']);
    // list records
    Route::get('/', [UsersController::class, 'index']);
    // one record
    Route::get('{id}', [UsersController::class, 'show']);
});

