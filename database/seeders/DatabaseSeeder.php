<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            \App\Modules\Admins\Database\Seeders\AdminSeeder::class,
            \App\Modules\Users\Database\Seeders\UserSeeder::class,
			\App\Modules\Guests\Database\Seeders\GuestSeeder::class,
			\App\Modules\Posts\Database\Seeders\PostSeeder::class,
			// DatabaseSeeds: DO NOT Remove This Line.
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
